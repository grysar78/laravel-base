#!/usr/bin/env bash

set -e

# Run our defined exec if args empty
if [ -z "$1" ]; then

    role=${CONTAINER_ROLE:-sitio}

    env=${LARAVEL_APP_ENV:-production}

    if [ "$role" = "backend" ]; then
        # cd /var/www && composer install; 
        # cd /var/www && php artisan cache:clear && php artisan config:clear && php artisan route:clear
        # echo "Cache cleared"

        # cd /var/www && php artisan migrate --force;    
        # echo "composer install migrate"
        
        echo "running apache..."        
        exec apache2-foreground
    fi

else

    exec "$@"

fi
