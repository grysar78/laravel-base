# Laravel - base app

Docker for Laravel 10/11 based on PHP apache 8.2

## Requirements

- Git

    https://git-scm.com/

- Docker y docker compose

    https://docs.docker.com/get-docker/

    https://docs.docker.com/compose/install/linux/

    https://docs.docker.com/compose/install/other/


## Compile & Run

```
docker compose build
docker compose up
```


